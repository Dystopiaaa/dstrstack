// Creation date: 22/08/2020 17:32:00

#ifdef INCLUDE_DSTR_TSTACK_H_9EE70BA2E48C11EAB1D69C2A7009E4B5

#include <stdbool.h>
#include <stddef.h>

#ifndef FIRST_TIME__INCLUDE_DSTR_TSTACK_H_9EE70BA2E48C11EAB1D69C2A7009E4B5
#define FIRST_TIME__INCLUDE_DSTR_TSTACK_H_9EE70BA2E48C11EAB1D69C2A7009E4B5

typedef enum E_DStrStack{
    E_DStrStack_IsEmpty,
    E_DStrStack_IsFull,
    E_DStrStack_TooSmallCapacity,
    E_DStrStack_NullAutoResizePtr,
    E_DStrStack_Allocation,
    E_DStrStack_Ok
} E_DStrStack;

#endif // FIRST_TIME__INCLUDE/DSTR/TSTACK_H_9EE70BA2E48C11EAB1D69C2A7009E4B5

/** 
 * Stack of elements of type INCLUDE/DSTR/_TYPE stored internaly in an array.
 * @invariant base <= top < end
 * @invariant \valid(base)
 * @invariant \valid(top)
 * @invariant end != NULL
 */
typedef struct DSTR_TYPED(DStrStack) DSTR_TYPED(DStrStack);

struct DSTR_TYPED(DStrStack) {
    /** Base of the stack */
    DSTR_TYPE *base;
    /** Top of the stack */
    DSTR_TYPE *top;
    /** End of the stack, the maximum address the top can reach */
    DSTR_TYPE *end;
    /** Function that resizes the stack by reallocating memory */
    E_DStrStack (* auto_resize) (DSTR_TYPED(DStrStack) *s);
};

typedef struct {

    /**
     * Initialises an empty stack, with a capacity greater than 0.
     * @requires \valid(s)
     * @requires internalArrayStart != NULL
     * @requires capacity > 0
     * @requires \valid(internalArrayStart + (0 .. capacity-1))
     * @ensures s->base = internalArrayStart
     * @ensures s->top = internalArrayStart
     * @ensures s->end = internalArrayStart + capacity
     * @assigns s->base, s->top, s->end
     */
    void (* const init) 
        (
         DSTR_TYPED(DStrStack) *s,
         DSTR_TYPE *internalArrayStart,
         ptrdiff_t capacity
        );

    /**
     * Initialises a stack with automatic memory management.
     * Doesn't allocate anything, requires to call auto_resize before push.
     * @requires \valid(s)
     * @ensures s->base == NULL
     * @ensures s->top == NULL
     * @ensures s->end == NULL
     * @ensures \valid(s->auto_resize)
     */
    void (* const auto_init) (DSTR_TYPED(DStrStack) *s);

    /**
     * Copy the value at the top of the stack.
     * The stack has to be not full.
     * Complexity is O(1)
     * @requires \valid_read(s)
     * @requires (s->top == \null && s->base == \null && s->end == \null) ||
     *      (\valid_read(s->base) &&\valid(s->top) && \valid_read(s->end))
     * behavior full:
     *      @assumes s->top == s->end
     *      @ensures \result == E_DStrStack_IsFull
     *      @assigns \nothing
     * behavior not_full:
     *      @assumes s->top != s->end
     *      @ensures \old(s->top) == s->top-1
     *      @ensures *(s->top) == value
     *      @assigns s->top
     * @complete behaviors
     * @disjoint behaviors
     */
    E_DStrStack (* const push)
        (
         DSTR_TYPED(DStrStack) *s,
         DSTR_TYPE value
        );

    /**
     * Remove the value at the top of the stack and copy it in out.
     * The stack has to be not empty.
     * Complexity is O(1)
     * @requires \valid_read(s)
     * @requires out == \null || \valid(out)
     * @requires (s->top == \null && s->base == \null && s->end == \null) ||
     *      (\valid_read(s->base) && \valid(s->top) && \valid_read(s->end))
     * @behavior not_empty:
     *      @assumes s->top != \null && s->top > s->base
     *      @ensures \old(s->top) = s->top+1
     *      @ensures \old(out != \null) ==> *out = *(s->top)
     *      @ensures \old(out == \null) ==> out == \null
     *      @ensures \result = E_DStrStack_Ok
     *      @assigns s->top, out
     * @behavior empty:
     *      @assumes s->top == \null || s->top <= s->base
     *      @ensures \result = E_DStrStack_IsEmpty
     *      @ensures *out = 0
     *      @assigns out
     * @complete behaviors
     * @disjoint behaviors
     */
    E_DStrStack (* const pop)
        (
         DSTR_TYPED(DStrStack) *s,
         DSTR_TYPE *out
        );

    /**
     * Return the maximum number of elements the stack could store.
     * Complexity is O(1)
     * @requires \valid_read(s)
     * @requires \valid_read(s->base, s->top, s->end) ||
     *      (s->base == \null && s->top == \null && s->end == \null)
     * @ensures s->base != NULL ==> \result = s->end - s->base
     * @ensures s->base != NULL ==> \result > 0
     * @ensures s->base == NULL ==> \result == 0
     * @assigns \nothing
     */
    ptrdiff_t (* const capacity) (DSTR_TYPED(DStrStack) *s);

    /**
     * Return the number of elements currently stored in the stack.
     * Complexity is O(1)
     * @requires \valid_read(s)
     * @requires \valid_read(s->base, s->top, s->end) ||
     *      (s->base == \null && s->top == \null && s->end == \null)
     * @ensures s->base != NULL ==> \result = s->top - s->base
     * @ensures s->base != NULL ==> \result > 0
     * @ensures s->base == NULL ==> \result == 0
     * @assigns \nothing
     */
    ptrdiff_t (* const size) (DSTR_TYPED(DStrStack) *s);

    /**
     * Return true iff the stack is full.
     * Complexity is O(1)
     * @requires \valid_read(s)
     * @requires \valid_read(s->base, s->top, s->end) ||
     * @ensures \result == (s->top == s->end)
     *      (s->base == \null && s->top == \null && s->end == \null)
     * @assigns \nothing
     */
    bool (* const is_full) (DSTR_TYPED(DStrStack) *s);

    /**
     * Return true iff the stack is empty.
     * Complexity is O(1)
     * @requires \valid_read(s)
     * @requires \valid_read(s->base, s->top, s->end) ||
     *      (s->base == \null && s->top == \null && s->end == \null)
     * @ensures \result == (s->base == s->top)
     * @assigns \nothing
     */
    bool (* const is_empty) (DSTR_TYPED(DStrStack) *s);

    /**
     * Resize the stack.
     * If s->base equals newInternalArray, don't copy the elements.
     * Otherwise copy elements in range [s->base ; s->end[ to newInternalArray.
     * Parameter newCapacity must be greater or equal than the size of s.
     * Complexity is O(n) if src->base != newInternalArray, O(1) otherwise.
     * @requires \valid(s)
     * @requires \valid(newInternalArray + (0 .. newCapacity-1)
     * @requires \valid(s->base + (0 .. ((s->end - s->base) - 1))
     * @requires newCapacity > 0
     * @requires \separated(
     *          newInternalArray + (0 .. newCapacity - 1),
     *          s->base + (0 .. s->end - s->base - 1)
     * @behavior newCapacityOk:
     *      @assumes s->base + newCapacity >= s->top
     *      @ensures s->base = newInternalArray
     *      @ensures s->top = s->top + (\old(s->top) - \old(s->base))
     *      @ensures s->end = s->base + newCapacity
     *      @ensures \forall ptrdiff_t i;
     *          0 <= i < (\old(s->top) - \old(s->base)) ==>
     *          *(\old(s->base) + i) = *(s->base + i)
     *      @ensures \result = E_DStrStack_Ok
     *      @assigns s->base, s->top, s->end
     * @behavior newCapacityTooSmall:
     *      @assumes s->base + newCapacity < s->top
     *      @ensures \result = E_DStrStack_TooSmallCapacity
     *      @assigns \nothing
     * @complete behaviors
     * @disjoint behaviors
     */
    E_DStrStack (* const resize)
        (
         DSTR_TYPED(DStrStack) *s,
         DSTR_TYPE *newInternalArray,
         ptrdiff_t newCapacity
        );

    /**
     * Double the capacity of the stack if it's full.
     * Divide the capacity of the stack by two if size/capacity < 0.375
     * @requires \valid(s)
     * @requires s->base == \null || \freeable(s->base)
     * @ensures \allocates(s->base) ==> \result == E_DStrStack_Ok
     * @ensures s->base == NULL ==> \result == E_DStrStack_Allocation
     * @ensures \valid(s->base)
     * @ensures \valid(s->top)
     * @ensures \valid(s->end)
     * @ensures s->end - s->base > 0
     * @ensures s->base <= s->top <= s->end
     * @assigns s->base, s->top, s->end
     */
    E_DStrStack (* const auto_resize) (DSTR_TYPED(DStrStack) *s);


} DSTR_TYPED(namespace_dstr_stack);

extern DSTR_TYPED(namespace_dstr_stack) const DSTR_TYPED(dstr_stack);

#endif // INCLUDE_DSTR_TSTACK_H_9EE70BA2E48C11EAB1D69C2A7009E4B5
