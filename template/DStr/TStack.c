// Creation date: 22/08/2020 17:31:58

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define AUTO_SHRINK_RATIO 0.375
#define MIN_CAPACITY 16

// ---------------------------------------------------------------------------//
// PRIVATE FUNCTIONS BEGIN
// ---------------------------------------------------------------------------//

E_DStrStack DSTR_TYPED(dstr_private_auto_resize) (DSTR_TYPED(DStrStack) *s);

// ---------------------------------------------------------------------------//
// PRIVATE FUNCTIONS END
// ---------------------------------------------------------------------------//

void DSTR_TYPED(dstr_stack_init) (
        DSTR_TYPED(DStrStack) *s,
        DSTR_TYPE *internalArrayStart,
        ptrdiff_t capacity)
{
    s->base = internalArrayStart;
    s->top = internalArrayStart;
    s->end = internalArrayStart+capacity;
    s->auto_resize = NULL;
}

void DSTR_TYPED(dstr_stack_auto_init) (DSTR_TYPED(DStrStack) *s){
    s->base = NULL;
    s->top = NULL;
    s->end = NULL;
    s->auto_resize = DSTR_TYPED(dstr_private_auto_resize);
}

E_DStrStack DSTR_TYPED(dstr_stack_push)
    (
     DSTR_TYPED(DStrStack) *s,
     DSTR_TYPE value
    )
{
    if(DSTR_TYPED(dstr_stack).is_full (s)){
        return E_DStrStack_IsFull;
    }

    *(s->top) = value;
    s->top++;

    return E_DStrStack_Ok;
}

E_DStrStack DSTR_TYPED(dstr_stack_pop)
    (
     DSTR_TYPED(DStrStack) *s,
     DSTR_TYPE *out
    )
{
    if(DSTR_TYPED(dstr_stack).is_empty (s)){
        if(out != NULL){
            *out = 0;
        }
        return E_DStrStack_IsEmpty;
    }

    s->top--;
    if(out != NULL){
        *out = *(s->top);
    }

    return E_DStrStack_Ok;
}

ptrdiff_t DSTR_TYPED(dstr_stack_capacity) (DSTR_TYPED(DStrStack) *s){
    if(s->base == NULL){
        return 0;
    }
    ptrdiff_t res = s->end - s->base;
    assert(res > 0);
    return res;
}

ptrdiff_t DSTR_TYPED(dstr_stack_size) (DSTR_TYPED(DStrStack) *s){
    if(s->base == NULL){
        return 0;
    }
    ptrdiff_t res = s->top - s->base;
    assert(res >= 0);
    return res;
}

bool DSTR_TYPED(dstr_stack_is_full) (DSTR_TYPED(DStrStack) *s){
    return s->top == s->end;
}

bool DSTR_TYPED(dstr_stack_is_empty) (DSTR_TYPED(DStrStack) *s){
    return s->base == s->top;
}

E_DStrStack DSTR_TYPED(dstr_stack_resize) (
        DSTR_TYPED(DStrStack) *s,
        DSTR_TYPE *newInternalArray,
        ptrdiff_t newCapacity
        )
{
    if(newCapacity < s->top - s->base){
        return E_DStrStack_TooSmallCapacity;
    }

    for(ptrdiff_t i=0 ; i<(s->top - s->base) ; i++){
        *(newInternalArray + i) = *(s->base + i);
    }

    //int size = s->top - s->base;
    //if(size > 0){
    //    memcpy(newInternalArray, s->base, size * sizeof(DSTR_TYPE));
    //}

    s->top = newInternalArray + (s->top - s->base);
    s->base = newInternalArray;
    s->end = newInternalArray + newCapacity;

    return E_DStrStack_Ok;
}

E_DStrStack DSTR_TYPED(dstr_private_auto_resize) (DSTR_TYPED(DStrStack) *s){
    E_DStrStack status = E_DStrStack_Ok;
    if(DSTR_TYPED(dstr_stack_is_full)(s)){
        ptrdiff_t capacity = DSTR_TYPED(dstr_stack_capacity)(s);
        if(capacity < MIN_CAPACITY){
            capacity = MIN_CAPACITY;
        }
        else{
            capacity *= 2;
        }

        DSTR_TYPE *oldBase = s->base;
        DSTR_TYPE *tmp = malloc(sizeof(DSTR_TYPE) * capacity);

        status = DSTR_TYPED(dstr_stack_resize) (s, tmp, capacity);

        free(oldBase);
    }
    else if(DSTR_TYPED(dstr_stack_capacity) (s) > MIN_CAPACITY){
        double ratio = DSTR_TYPED(dstr_stack_size)(s);
        ratio /= DSTR_TYPED(dstr_stack_capacity)(s);

        if(ratio < AUTO_SHRINK_RATIO){
            ptrdiff_t capacity = DSTR_TYPED(dstr_stack_capacity)(s);
            capacity /= 2;
            
            DSTR_TYPE *oldBase = s->base;
            DSTR_TYPE *tmp = malloc(sizeof(DSTR_TYPE) * capacity);

            status = DSTR_TYPED(dstr_stack_resize) (s, tmp, capacity);
            free(oldBase);
        }
    }
    return status;
}

E_DStrStack DSTR_TYPED(dstr_stack_auto_resize) (DSTR_TYPED(DStrStack) *s){
    if(s->auto_resize == NULL){
        return E_DStrStack_NullAutoResizePtr;
    }

    s->auto_resize(s);

    return E_DStrStack_Ok;
}


DSTR_TYPED(namespace_dstr_stack) const DSTR_TYPED(dstr_stack) = {
    .init        = &DSTR_TYPED(dstr_stack_init),
    .auto_init   = &DSTR_TYPED(dstr_stack_auto_init),
    .push        = &DSTR_TYPED(dstr_stack_push),
    .pop         = &DSTR_TYPED(dstr_stack_pop),
    .capacity    = &DSTR_TYPED(dstr_stack_capacity),
    .size        = &DSTR_TYPED(dstr_stack_size),
    .is_full     = &DSTR_TYPED(dstr_stack_is_full),
    .is_empty    = &DSTR_TYPED(dstr_stack_is_empty),
    .resize      = &DSTR_TYPED(dstr_stack_resize),
    .auto_resize = &DSTR_TYPED(dstr_stack_auto_resize)
};
