// Creation date: 22/08/2020 18:43:18

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "DStr/IntegerStack.h"

int test_run = 0;

#define FAIL() printf("\nfailure in %s() line %d\n", __func__, __LINE__)
#define _assert(test) do { if (!(test)) { FAIL(); return 1; } } while(0)
#define _verify(test) do { int r=test(); test_run++; if(r) return r; } while(0)

int test_init(){
    DStrStack_int s;


    int size = 10;
    int arr[10];
    dstr_stack_int.init(&s, arr, 10);

    _assert(s.base == arr);
    _assert(s.top == arr);
    _assert(s.end == arr+10);
    return 0;
}

int test_empty(){
    DStrStack_int s;

    int arr[10];
    dstr_stack_int.init(&s, arr, 10);

    _assert(dstr_stack_int.is_empty(&s));

    *(s.top) = 20;
    s.top++;
    _assert(!dstr_stack_int.is_empty(&s));

    return 0;
}

int test_is_full(){
    DStrStack_int s;

    int arr[10];
    dstr_stack_int.init(&s, arr, 10);

    _assert(!dstr_stack_int.is_full(&s));

    for(int *elem = s.base; elem != s.end; elem++){
        *elem = 0;
    }

    s.top = s.end;
    
    _assert(dstr_stack_int.is_full(&s));
    
    s.top--;
    _assert(!dstr_stack_int.is_full(&s));

    return 0;
}

int test_size(){
    DStrStack_int s;

    int arr1[1];
    dstr_stack_int.init(&s, arr1, 1);
    _assert(0 == dstr_stack_int.size(&s));

    *(s.top) = 0;
    s.top++;
    
    _assert(1 == dstr_stack_int.size(&s));
    
    int arr2[10];
    dstr_stack_int.init(&s, arr2, 10);

    for(int i=0 ; i<3 ; i++){
        *(s.top) = 0;
        s.top++;
    }

    _assert(3 == dstr_stack_int.size(&s));

    return 0;
}

int test_capacity(){
    DStrStack_int s;

    int arr1[1];
    dstr_stack_int.init(&s, arr1, 1);
    _assert(1 == dstr_stack_int.capacity(&s));

    int arr2[200];
    dstr_stack_int.init(&s, arr2, 200);
    _assert(200 == dstr_stack_int.capacity(&s));

    return 0;
}

int test_pop(){
    DStrStack_int s;

    int arr[10];
    dstr_stack_int.init(&s, arr, 3);

    int value = 400;
    _assert(E_DStrStack_IsEmpty == dstr_stack_int.pop(&s, &value));
    _assert(value == 0);

    *(s.top) = 84;
    s.top++;

    *(s.top) = 73;
    s.top++;

    *(s.top) = -20;
    s.top++;

    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == -20);

    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 73);

    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 84);
    
    _assert(E_DStrStack_IsEmpty == dstr_stack_int.pop(&s, &value));
    _assert(value == 0);

    return 0;
}

int test_pop__null_out_param(){
    DStrStack_int s;

    int arr[10];
    dstr_stack_int.init(&s, arr, 3);

    *(s.top) = 84;
    s.top++;

    _assert(s.top == s.base+1);
    dstr_stack_int.pop(&s, NULL);

    _assert(s.top == s.base);
    return 0;
}

int test_push(){
    DStrStack_int s;

    int arr[3];
    dstr_stack_int.init(&s, arr, 3);

    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 20));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 21));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 22));
    _assert(E_DStrStack_IsFull == dstr_stack_int.push(&s, 23));

    _assert(s.top == s.end);
    s.top--;
    _assert(22 == *(s.top));
    s.top--;
    _assert(21 == *(s.top));
    s.top--;
    _assert(20 == *(s.top));
    _assert(s.top == s.base);
    return 0;
}

int test_resize__no_malloc(){
    DStrStack_int s;

    int arr1[4];
    int arr2[8];

    dstr_stack_int.init(&s, arr1, 4);

    *(s.top) = 20; s.top++;
    *(s.top) = 21; s.top++;
    *(s.top) = 22; s.top++;
    *(s.top) = 23; s.top++;

    _assert(4 == s.end - s.base);

    _assert(E_DStrStack_Ok == dstr_stack_int.resize(&s, arr2, 8));

    _assert(8 == s.end - s.base);

    *(s.top) = 24; s.top++;
    *(s.top) = 25; s.top++;
    *(s.top) = 26; s.top++;
    *(s.top) = 27; s.top++;

    s.top--; *(s.top) == 27;
    s.top--; *(s.top) == 26;
    s.top--; *(s.top) == 25;
    s.top--; *(s.top) == 24;
    s.top--; *(s.top) == 23;
    s.top--; *(s.top) == 22;
    s.top--; *(s.top) == 21;
    s.top--; *(s.top) == 20;
    
    _assert(E_DStrStack_Ok == dstr_stack_int.resize(&s, arr1, 2));
    
    *(s.top) = 20; s.top++;
    *(s.top) = 21; s.top++;
    
    _assert(E_DStrStack_TooSmallCapacity == dstr_stack_int.resize(&s, arr1, 1));
    return 0;
}

int test_resize__malloc(){
    DStrStack_int s;

    int size = 4;
    int *arr = malloc(sizeof(int) * size);
    int *prev = NULL;

    dstr_stack_int.init(&s, arr, size);
    _assert(0 == dstr_stack_int.size(&s));
    _assert(dstr_stack_int.capacity(&s) == size);

    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 10));
    _assert(1 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 20));
    _assert(2 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 30));
    _assert(3 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 40));
    _assert(4 == dstr_stack_int.size(&s));

    size *= 2;
    prev = arr;
    arr = malloc(sizeof(int) * size);
    dstr_stack_int.resize(&s, arr, size);
    free(prev);
    prev = NULL;
    _assert(dstr_stack_int.capacity(&s) == size);
    
    _assert(4 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 50));
    _assert(5 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 60));
    _assert(6 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 70));
    _assert(7 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 80));
    _assert(8 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_IsFull == dstr_stack_int.push(&s, 90));
    _assert(8 == dstr_stack_int.size(&s));
    
    int value = 96;
    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 80);
    _assert(7 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 70);
    _assert(6 == dstr_stack_int.size(&s));
    
    size -= 2;
    prev = arr;
    arr = malloc(sizeof(int) * size);
    dstr_stack_int.resize(&s, arr, size);
    free(prev);
    prev = NULL;
    _assert(dstr_stack_int.capacity(&s) == size);
    
    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 60);
    _assert(5 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 50);
    _assert(4 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 40);
    _assert(3 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 30);
    _assert(2 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 20);
    _assert(1 == dstr_stack_int.size(&s));
    _assert(E_DStrStack_Ok == dstr_stack_int.pop(&s, &value));
    _assert(value == 10);
    _assert(0 == dstr_stack_int.size(&s));

    free(arr);
    arr = NULL;

    return 0;
}

int test_resize__null_auto_resize(){
    DStrStack_int s;

    int capacity = 16;
    int arr[capacity];
    dstr_stack_int.init(&s, arr, capacity);

    for(ptrdiff_t i=0 ; i<capacity ; i++){
        _assert(E_DStrStack_Ok == dstr_stack_int.push(&s, 1));
        _assert(E_DStrStack_NullAutoResizePtr == dstr_stack_int.auto_resize(&s));
    }
    _assert(E_DStrStack_IsFull == dstr_stack_int.push(&s, 1));
    return 0;
}

int test_auto_init__normal(){
    DStrStack_int s;

    dstr_stack_int.auto_init(&s);

    _assert(dstr_stack_int.size(&s) == 0);
    _assert(dstr_stack_int.capacity(&s) == 0);
    _assert(dstr_stack_int.is_empty(&s));
    _assert(dstr_stack_int.is_full(&s));
    int val = 1;
    _assert(dstr_stack_int.pop(&s, &val) == E_DStrStack_IsEmpty);
    _assert(val == 0);
    _assert(dstr_stack_int.push(&s, 10) == E_DStrStack_IsFull);

    free(s.base);
    s = (const DStrStack_int) { 0 };
    return 0;
}

int test_auto_init__grow(){
    DStrStack_int s;

    dstr_stack_int.auto_init(&s);

    ptrdiff_t capacity = 0;

    for(int i=0 ; i<10 ; i++){
        _assert(dstr_stack_int.auto_resize(&s) == E_DStrStack_Ok);
        _assert(!dstr_stack_int.is_full(&s));
        _assert(capacity < dstr_stack_int.capacity(&s));
        capacity = dstr_stack_int.capacity(&s);

        while(dstr_stack_int.size(&s) != dstr_stack_int.capacity(&s)){
            _assert(!dstr_stack_int.is_full(&s));

            _assert(dstr_stack_int.push(&s, 10));
        }
    }

    free(s.base);
    s = (const DStrStack_int) { 0 };
    return 0;
}

int test_auto_init__shrink(){
    DStrStack_int s;

    dstr_stack_int.auto_init(&s);
    _assert(dstr_stack_int.auto_resize(&s) == E_DStrStack_Ok);

    ptrdiff_t min_capacity = dstr_stack_int.capacity(&s);

    while(dstr_stack_int.size(&s) != dstr_stack_int.capacity(&s)){
        _assert(dstr_stack_int.push(&s, 10) == E_DStrStack_Ok);
    }

    while(dstr_stack_int.size(&s) > 0){
        int value = 0;
        _assert(dstr_stack_int.pop(&s, &value) == E_DStrStack_Ok);
    }

    _assert(dstr_stack_int.size(&s) == 0);
    _assert(dstr_stack_int.capacity(&s) == min_capacity);

    while(dstr_stack_int.size(&s) != dstr_stack_int.capacity(&s)){
        _assert(dstr_stack_int.push(&s, 10) == E_DStrStack_Ok);
    }

    _assert(dstr_stack_int.auto_resize(&s) == E_DStrStack_Ok);
    _assert(dstr_stack_int.capacity(&s) == 2 * min_capacity);

    while(dstr_stack_int.size(&s) != dstr_stack_int.capacity(&s)){
        _assert(dstr_stack_int.push(&s, 10) == E_DStrStack_Ok);
    }

    while(dstr_stack_int.size(&s)/(double)dstr_stack_int.capacity(&s) >= 0.375){
        int val = 0;
        _assert(dstr_stack_int.capacity(&s) == 2 * min_capacity);
        _assert(dstr_stack_int.pop(&s, &val) == E_DStrStack_Ok);
    }
    
    _assert(dstr_stack_int.auto_resize(&s) == E_DStrStack_Ok);
    _assert(dstr_stack_int.capacity(&s) == min_capacity);
    
    free(s.base);
    s = (const DStrStack_int) { 0 };

    return 0;
}

int all_tests(){
    _verify(test_init);
    _verify(test_empty);
    _verify(test_is_full);
    _verify(test_size);
    _verify(test_capacity);
    _verify(test_pop);
    _verify(test_push);
    _verify(test_resize__no_malloc);
    _verify(test_resize__malloc);
    _verify(test_resize__null_auto_resize);
    _verify(test_auto_init__normal);
    _verify(test_auto_init__grow);
    _verify(test_auto_init__shrink);
    _verify(test_pop__null_out_param);
    return 0;
}

int main(void){
    int result = all_tests();
    if(result == 0){
        printf("PASSED\n");
    }

    printf("Test run: %d\n", test_run);
    return result != 0;
}
