// Creation date: 22/08/2020 19:12:08

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "DStr/IntegerStack.h"
#include "DStr/DoubleStack.h"

#define SIZE 20

int main(void){
    {
        DStrStack_int stack;

        int arr[SIZE];
        dstr_stack_int.init(&stack, arr, SIZE);

        for(ptrdiff_t i=0; i<SIZE ; i++){
            if(dstr_stack_int.push(&stack, i) != E_DStrStack_Ok){
                fprintf(stderr, "Can't push one more item in the stack !\n");
                return EXIT_FAILURE;
            }
            
            printf("Size: %ld\n", dstr_stack_int.size(&stack));
            printf("Capacity: %ld\n", dstr_stack_int.capacity(&stack));
        }

        for(ptrdiff_t i=0; i<SIZE ; i++){
            int value = 0;

            if(dstr_stack_int.pop(&stack, &value) != E_DStrStack_Ok){
                fprintf(stderr, "Can't pop one more item from the stack !\n");
                return EXIT_FAILURE;
            }

            //printf("%d ", value);
        }
        printf("\n");
    }

    {
        DStrStack_double stackd;
        double arrd[SIZE];
        dstr_stack_double.init(&stackd, arrd, SIZE);

        for(ptrdiff_t i=0; i<SIZE ; i++){
            if(dstr_stack_double.push(&stackd, sqrt(i+1)) != E_DStrStack_Ok){
                fprintf(stderr, "Can't push one more item from the stack !\n");
                return EXIT_FAILURE;
            }
        }

        for(ptrdiff_t i=0; i<SIZE ; i++){
            double value = 0;
            if(dstr_stack_double.pop(&stackd, &value) != E_DStrStack_Ok){
                fprintf(stderr, "Can't push one more item from the stack !\n");
                return EXIT_FAILURE;
            }

            printf("%f ", value);
        }
        printf("\n");
    }

    {
        DStrStack_int stackTest;

        ptrdiff_t sizeTest = 1;
        assert(sizeTest > 0);
        int *arrTest = malloc(sizeof(*arrTest) * sizeTest);

        dstr_stack_int.init(&stackTest, arrTest, sizeTest);

        ptrdiff_t bound = (ptrdiff_t) 1*pow(1024, 2);
        for(ptrdiff_t i=0 ; i<bound ; i++){
            if(dstr_stack_int.is_full(&stackTest)){
                int *tmp = arrTest;
                sizeTest *= 2;
                assert(sizeTest > 0);
                arrTest = malloc(sizeof(*arrTest) * sizeTest);
                if(arrTest == NULL){
                    fprintf(stderr, "Can't do that !\n");
                }
                dstr_stack_int.resize(&stackTest, arrTest, sizeTest);
                free(tmp);
            }

            dstr_stack_int.push(&stackTest, i%100);
        }

        printf("Alloca %f B !\n",(sizeof(*arrTest)*sizeTest)/(pow(1024, 0)));
        printf("Alloca %f KiB !\n",(sizeof(*arrTest)*sizeTest)/(pow(1024, 1)));
        printf("Alloca %f MiB !\n",(sizeof(*arrTest)*sizeTest)/(pow(1024, 2)));
        printf("Alloca %f GiB !\n",(sizeof(*arrTest)*sizeTest)/(pow(1024, 3)));

        free(arrTest);
        arrTest = NULL;
        stackTest = (const DStrStack_int) { 0 };
        assert(stackTest.base == NULL);
        assert(stackTest.top == NULL);
        assert(stackTest.end == NULL);
        sizeTest = 0;
    }

    {
        DStrStack_double stack = { 0 };

        dstr_stack_double.auto_init(&stack);

        for(int i=0 ; i<100 ; i++){
            dstr_stack_double.auto_resize(&stack);
            dstr_stack_double.push(&stack, i);
        }

        for(int i=0 ; i<100 ; i++){
            if(i != 0){
                printf(" ");
            }
            double value = 0;
            dstr_stack_double.pop(&stack, &value);
            printf("%f", value);
        }
        printf("\n");

        free(stack.base);
        stack = (const DStrStack_double) { 0 };
    }

    return EXIT_SUCCESS;
}
