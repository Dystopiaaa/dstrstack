# DStrStack
Generic stack implementation in C

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Related projects](#related-projects)
* [Contact](#contact)

## General info
The goal of this project is to create a stack implementation that can be used
with any type.

Functions behavior will be specified using ACSL. Then the WP plug-in
of Frama-C will be used to prove that the implementation of those functions
match their specification.

Note that Frama-C with WP plug-in can't prove that the specification of a
function is correct, so bugs can be found here even if all the function
implementation match their specification according to Frama-C.

This project should not be used in production for now, see the
[Related projects](#related-projects) section if you need reliable data 
structures such as stacks in C.

## Technologies
* Language: [C11](https://en.wikipedia.org/wiki/C11_%28C_standard_revision%29)
* Build automation: [CMake](https://cmake.org/)
* Version control: [Git](https://en.wikipedia.org/wiki/Git)
* Program proof: [Frama-C](https://frama-c.com/)
* Specification: [ACSL](https://frama-c.com/acsl.html)
* **W**eakest **P**recondition calculus: [WP plug-in](
https://frama-c.com/wp.html) 

## Setup
To build the project, you can follow the following steps:

Clone the repository:

`git clone "https://gitlab.com/Dystopiaaa/dstrstack"`

Create the script that will compile the project:

`cd dstrstack/bin`

`cmake ..`

Launch the script:

`cmake --build .`

Launch the tests to check that everything is ok:

`ctest`

## Code Examples
No code example yet

## Features
List of features ready and TODOs for future development

* Generic stack implementation using resizable array (not linked list)
* Public functions are in "namespaces" created with C structures

To-do list:

* Prove that the stack implementation matches ACSL specification in Frama-C
* Create a demo code that shows how to use the stack
* Add a library creation to CMakeLists.txt
* Fill the Code Examples section of README.md

## Status
Project is: _in progress_

## Inspiration
This project was inspired by the following stackoverflow questions

* [Namespaces in C](
https://stackoverflow.com/questions/389827/namespaces-in-c)
* [Simulation of templates in C](
https://stackoverflow.com/questions/10950828/simulation-of-templates-in-c-for-a-queue-data-type)

Related projects were found [here](
https://stackoverflow.com/questions/668501/are-there-any-open-source-c-libraries-with-common-data-structures)
.

## Related projects
This is a non exhaustive list of projects that offers data structures in C

* [Glib](https://developer.gnome.org/glib/stable/glib-data-types.html) is a
cross platform library that provides among other things data structures such as
single and double linked list or double ended queues under the [LGPLv2.1](
https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License) license.
* [queue.h](https://github.com/freebsd/freebsd/blob/master/sys/sys/queue.h) and
 [tree.h](https://github.com/freebsd/freebsd/blob/master/sys/sys/tree.h) from
 [FreeBSD](https://www.freebsd.org/) are two header file libraries containing
 macros to create list, queue and tree under the [BSD](
https://en.wikipedia.org/wiki/BSD_licenses) license.
* [Gnulib](https://www.gnu.org/software/gnulib/) is a library providing among
other things a lot of [data structures](
https://www.gnu.org/software/gnulib/manual/html_node/Container-data-types.html#Container-data-types)
 such as list, ordered and unordered set, and tree under the [GPL](
https://en.wikipedia.org/wiki/GNU_General_Public_License) license.
* [Klib](https://github.com/attractivechaos/klib) provides tree, list,
 dynamic array, hash table and deque under the [MIT/X11 license](
https://en.wikipedia.org/wiki/MIT_License).

## Contact
Feel free to contact me at dystopiaaa@laposte.net.
